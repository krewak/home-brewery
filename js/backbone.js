(function() {

	var Beer = Backbone.Model.extend();

	var Ingredient = Backbone.Model.extend({
		defaults: {
			"count": 1,
		},
		getNameWithCount: function() {
			if(this.get("count") > 1) {
				return this.get("name") + " (" + this.get("count") + ")";
			} else {
				return this.get("name");
			}
		}
	});

	var BeerView = Backbone.View.extend({
		tagName: "tr",
		events: {
			"click .table-cell": "expand",
			"click .collapse-item": "close",

		},
		initialize: function() {
			_.bindAll(this, "render");
		},
		generateImageId: function(id) {
			if(id < 10) {
				return "00" + id;
			} else if(id < 100) {
				return "0" + id;
			} else {
				return id;
			}
		},
		render: function(e) {
			var data = {
				id: this.model.get("id"),
				image_id: this.generateImageId(this.model.get("id")),
				name: this.model.get("name"),
				style: this.model.get("style"),
				abv: this.model.get("abv"),
				blg: this.model.get("blg"),
				ibu: this.model.get("ibu"),
				brewingdate: this.model.get("brewing_date"),
			};
			var beerRow = _.template($("#beer-row-template").html());
			this.$el.html(beerRow(data));
			return this;
		},
		expand: function() {
			var description = "";
			$.ajax({ 
				url: "data/descriptions/" + this.model.get("id"), 
				async: false,
				dataType: "html",
				success: function(data) {
					description = data;
				}
			});

			var recipe = "";
			$.ajax({ 
				url: "data/recipes/" + this.model.get("id"), 
				async: false,
				dataType: "html",
				success: function(data) {
					recipe = data;
				}
			});

			var data = {
				id: this.model.get("id"),
				label: this.model.get("label"),
				name: this.model.get("name"),
				style: this.model.get("style"),

				abv: this.model.get("abv"),
				blg: this.model.get("blg"),
				ibu: this.model.get("ibu"),

				malts: this.model.get("malts").join(", "),
				nonfermentables: this.model.get("nonfermentables").join(", "),
				hops: this.model.get("hops").join(", "),
				yeast: this.model.get("yeasts").join(", "),
				others: this.model.get("others").join(", "),

				malts_count: this.model.get("malts").length,
				hops_count: this.model.get("hops").length,

				description: description,
				recipe: recipe,

				brewingdate: this.model.get("brewing_date"),
				bottlingdate: this.model.get("bottling_date"),
				status: this.model.get("status"),
			};

			this.$el.find("td").addClass("hidden");

			var description = _.template($("#beer-desc-template").html());
			this.$el.append(description(data));

			return this;
		},
		close: function() {
			this.$el.find("td").removeClass("hidden");
			this.$el.find("td:not(.table-cell)").remove();
		}
	});

	var BeerList = Backbone.Collection.extend({
		model: Beer,
		url: "data/batches.json",
		comparator: function(beer) {
			return -beer.get("id");
		}
	});

	var IngredientsList = Backbone.Collection.extend({
		comparator: function(ingredient) {
			return ingredient.get("name").toLowerCase();
		}
	});

	var HopList = IngredientsList.extend();
	var MaltList = IngredientsList.extend();
	var YeastList = IngredientsList.extend();
	var OtherList = IngredientsList.extend();
	var NonfermentablesList = IngredientsList.extend();
	var StyleList = IngredientsList.extend();

	var BeerListView = Backbone.View.extend({
		el: $("#brewed-beers > tbody"),
		initialize: function() {			
			this.beers = new BeerList();

			this.hops = new HopList();
			this.malts = new MaltList();
			this.yeasts = new YeastList();
			this.others = new OtherList();
			this.nonfermentables = new NonfermentablesList();
			this.styles = new StyleList();

			this.filter = null;
			this.filter_name = null;

			var self = this;
			this.beers.bind("add", this.loadData, this);
			this.beers.fetch({
				success: function() {
					self.setNewOrder();
					self.hops.sort();
					self.malts.sort();
					self.yeasts.sort();
					self.others.sort();
					self.nonfermentables.sort();
					self.styles.sort();

					self.hops.each(function(hop) {
						$("#hops").append($("<option></option>").text(hop.getNameWithCount()).attr("value", hop.get("name")));
					});
					self.malts.each(function(malt) {
						$("#malts").append($("<option></option>").text(malt.getNameWithCount()).attr("value", malt.get("name")));
					});
					self.yeasts.each(function(yeast) {
						$("#yeasts").append($("<option></option>").text(yeast.getNameWithCount()).attr("value", yeast.get("name")));
					});
					self.others.each(function(other) {
						$("#others").append($("<option></option>").text(other.getNameWithCount()).attr("value", other.get("name")));
					});
					self.nonfermentables.each(function(nonfermentable) {
						$("#nonfermentables").append($("<option></option>").text(nonfermentable.getNameWithCount()).attr("value", nonfermentable.get("name")));
					});
					self.styles.each(function(style) {
						$("#styles").append($("<option></option>").text(style.getNameWithCount()).attr("value", style.get("name")));
					});

					// hopSummaryView.loadData(self.beers, self.hops);
					self.render();

					var router = new AppRouter();
					Backbone.history.start();
				}
			});
		},
		render: function() {
			this.$el.empty();
			var self = this;

			this.beers.each(function(beer) {
				var beerView = new BeerView({
					model: beer
				});

				self.$el.append(beerView.render().el);
			});
		},
		sortBy: function(sort, order, isInteger) {
			if(isInteger) {
				this.beers.comparator = function(model) {
					return order * model.get(sort);
				}
			} else {
				this.beers.comparator = function(a, b) {
					return order * a.get(sort).localeCompare(b.get(sort));
				}
			}

			this.beers.sort();
			if(this.filter && this.filter_name) {
				this.filterBy(this.filter, this.filter_name);
			} else {
				this.render();
			}
		},
		filterBy: function(filter, name) {
			this.$el.empty();
			var self = this;

			this.filter = filter;
			this.filter_name = name;

			this.beers.each(function(beer) {
				if(beer.get(filter).indexOf(name) > -1) {
					var beerView = new BeerView({
						model: beer
					});
					self.$el.append(beerView.render().el);
				}
			});
		},
		reset: function() {
			this.filter = null;
			this.filter_name = null;

			this.beers.comparator = function(model) {
				return -model.get("id");
			}

			this.beers.sort();
			this.render();
		},
		loadData: function(beer) {
			this.manageIngredients(this.hops, beer.get("hops"));
			this.manageIngredients(this.malts, beer.get("malts"));
			this.manageIngredients(this.yeasts, beer.get("yeasts"));
			this.manageIngredients(this.others, beer.get("others"));
			this.manageIngredients(this.nonfermentables, beer.get("nonfermentables"));
			this.manageIngredients(this.styles, beer.get("styles"));
		},
		manageIngredients: function(collection, data) {
			data.forEach(function(ingredient) {
				if(!collection.where({name: ingredient}).length) {
					collection.add(new Ingredient({
						name: ingredient
					}));
				} else {
					var model = collection.find(function(model) { return model.get("name") == ingredient; });
					model.set("count", model.get("count") + 1);
				}
			});
		},
		collapseAll: function() {
			$(".collapse-item").click();
		},
		setNewOrder: function() {
			this.hops.comparator = function(model) {
				return -model.get("count");
			}
			this.malts.comparator = function(model) {
				return -model.get("count");
			}
			this.yeasts.comparator = function(model) {
				return -model.get("count");
			}
			this.others.comparator = function(model) {
				return -model.get("count");
			}
			this.nonfermentables.comparator = function(model) {
				return -model.get("count");
			}
			this.styles.comparator = function(model) {
				return -model.get("count");
			}
		},
		scrollAndOpen: function(id) {
			this.$el.empty();
			var self = this;

			this.beers.each(function(beer) {
				var beerView = new BeerView({
					model: beer
				});

				self.$el.append(beerView.render().el);
				if(beer.get("id") == id) {
					$("html, body").animate({
						scrollTop: $("#beer-id-" + id).parent().offset().top
					}, 1000);
					beerView.expand();
				}
			});
		}
	});

	var TableHeaderView = Backbone.View.extend({
		el: $("#brewed-beers > thead"),
		events: {
			"click i": "sort"
		},
		initialize: function() {
			this.$el.find("th.sortable").append(" <small><i class=\"fa fa-caret-up\"></i><i class=\"fa fa-caret-down\"></i></small>");
		},
		sort: function(e) {
			var sort = $(e.target).closest("th").data("sort");
			var order = $(e.target).hasClass("fa-caret-down") ? -1 : 1;
			var isInteger = $(e.target).closest("th").data("int");

			listView.sortBy(sort, order, isInteger);
		}
	});

	var SettingsView = Backbone.View.extend({
		el: $("#settings"),
		events: {
			"click #collapse": "collapse",
			"click #reset": "reset"
		},
		initialize: function() {
			$("[data-toggle=\"tooltip\"]").tooltip()
		},
		reset: function() {
			listView.reset();
			$("#hops, #malts, #yeasts, #others, #nonfermentables, #styles").prop("selectedIndex", 0);
			history.pushState("", document.title, window.location.pathname);
		},
		collapse: function() {
			listView.collapseAll();
		}
	});

	var SelectView = Backbone.View.extend({
		events: {
			"change": "filter",
		},
		elements: ["hops", "malts", "yeasts", "others", "nonfermentables", "styles"],
		filter: function(e) {
			listView.filterBy(this.filter_name, e.target.value);

			var filter_name = this.filter_name;
			this.elements.forEach(function(element) {
				if(element != filter_name) {
					$("#" + element).prop("selectedIndex", 0);
				}
			});
		}
	});

	var HopSelectView = SelectView.extend({
		el: $("#hops"),
		filter_name: "hops",
	});

	var MaltSelectView = SelectView.extend({
		el: $("#malts"),
		filter_name: "malts",
	});

	var YeastSelectView = SelectView.extend({
		el: $("#yeasts"),
		filter_name: "yeasts",
	});

	var OtherSelectView = SelectView.extend({
		el: $("#others"),
		filter_name: "others",
	});

	var NonfermentablesSelectView = SelectView.extend({
		el: $("#nonfermentables"),
		filter_name: "nonfermentables",
	});

	var StylesSelectView = SelectView.extend({
		el: $("#styles"),
		filter_name: "styles",
	});

	// var HopSummaryView = Backbone.View.extend({
	// 	el: $("#used-hops"),
	// 	loadData: function(beers, hops) {
	// 		var el = this.$el;
	// 		el.find("thead").append("<th></th>");
	// 		hops.each(function(ingredient) {
	// 			el.find("thead").append("<th class=\"rotate\"><div><span>" + ingredient.get("name") + "</span></div></th>");
	// 		});

	// 		beers.each(function(beer) {
	// 			var row = el.find("tbody").append("<tr>");
	// 			row.append("<td>" + beer.get("name") + "</td>");
	// 			hops.each(function(ingredient) {
	// 				row.append("<td>");
	// 				if(beer.get("hops").indexOf(ingredient.get("name")) > -1) {
	// 					row.append("<i class=\"fa fa-check\"></i>");	
	// 				}
	// 				row.append("</td>");
	// 			});
	// 			el.find("tbody").append("</tr>");
	// 		});
	// 	}
	// });

	AppRouter = Backbone.Router.extend({
		routes: {
			"beer/:id": "show",
		},
		show: function(id) {
			var model = listView.beers.find(function(model) { return model.get("id") == id; });

			if(model) {
				$(".preload-screen").remove();
				$("html, body").css({
					"overflow": "auto",
					"height": "auto"
				});

				setTimeout(function(){
					listView.scrollAndOpen(model.get("id"));
				}, 500);
			}

		}
	});

	var listView = new BeerListView();
	var tableHeaderView = new TableHeaderView();

	var settingsView = new SettingsView();
	var hopSelectView = new HopSelectView();
	var maltSelectView = new MaltSelectView();
	var yeastSelectView = new YeastSelectView();
	var otherSelectView = new OtherSelectView();
	var nonfermentablesSelectView = new NonfermentablesSelectView();
	var stylesSelectView = new StylesSelectView();

	// var hopSummaryView = new HopSummaryView();

})();