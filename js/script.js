$(window).load(function() {

	$(".preload-screen").fadeOut("slow");
	$("html, body").css({
		"overflow": "auto",
		"height": "auto"
	});

	if(!location.hash) {
		$("html, body").animate({ 
			scrollTop: 0 
		});
	}

});

$(document).ready(function() {
	
	function formatText($el) {
		$el.each(function() {
			var conjunctions = ["a", "i", "o", "u", "w", "z", "W"];
			var div = $(this);

			conjunctions.forEach(function(conj) {
				var str = div.html();
				if(str.indexOf(" " + conj + " ") > -1) {
					var conv = str.replace(new RegExp(" " + conj + " ", 'g'), " " + conj + "&nbsp;");
					div.html(conv);
				}
			});
		});
	}

	formatText($("p.formatted-text"));

	$("#email-form > input, #email-form > textarea, #email-form > button").click(function() {
		$("#noEmailWarning").modal("show");
	});

});

if(!location.hash) {

	pages = $(".page > .page-container > h1");
	pages.parent().fadeTo(0, 0);

	$(window).scroll(function() {
		pages.each(function() {
			if($(this).offset().top + $(this).height() < $(window).scrollTop() + $(window).height()) {
				$(this).parent().fadeTo(1500, 1);
			}
		});
	});

}